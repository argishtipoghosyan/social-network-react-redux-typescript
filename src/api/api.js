import * as Axios from 'axios';

const instance = Axios.create({
    withCredentials: true,
    baseURL:'https://social-network.samuraijs.com/api/1.0/',
    headers:{"API-KEY":"e8a8bb25-10e8-45a3-a32f-46dcdd87be30"},

})

export const usersAPI = {
    getUsers(currentPage, pageSize) {
        return  instance.get(`users?page=${currentPage}&count=${pageSize}`)
        .then(Response =>{
                return Response.data
            })
    },
    unfollow(userId) {
        return instance.delete(`follow/${userId}`)
    },
    follow(userId) {
        return instance.post(`follow/${userId}`)
    },
    getProfile(userId) {
        return profileAPI.getProfile(userId)
    }
}

export const profileAPI = {
    getProfile(userId) {
        return instance.get(`profile/${userId}`)
    },
    getStatus(userId) {
        return instance.get(`profile/status/${userId}`)
    },
    updateStatus(status) {
        return instance.put(`profile/status` , { status:status })
    },
    savePhoto(photoFile) {
        const formData = new FormData()  
        formData.append("image",photoFile)
        return instance.put(`profile/photo` ,formData,{
            headers:{
                'Content-Type':'multipart/from-data'
            }
        })
    },
    saveProfile(profile) {
        return instance.put(`profile`,profile)
    },
}
export const authAPI = {
    me() {
        return instance.get(`auth/me`)
    },
    login(email ,password, rememberMe = false, captcha = null) {
        return instance.post(`auth/login`,{ email ,password, rememberMe, captcha })
    },
    logout() {
        return instance.delete(`auth/login`)
    }
}

export const securityAPI = {
    getCaptchaUrl() {
        return instance.get(`security/get-captcha-url`)
    }
}