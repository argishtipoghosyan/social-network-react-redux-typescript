import React from 'react';
import './App.css';
import { Route, BrowserRouter, withRouter } from 'react-router-dom';
import Navbar from './Components/Navbar/Navbar';
import UsersConteiner from './Components/Users/UsersConteiner';
import HeaderConteiner from './Components/Header/HeaderConteiner';
import LoginPage from './Components/Login/Login';
import { connect } from 'react-redux';
import { compose } from 'redux';
import { initializeApp } from './Redux/app-reducer';
import Preloader from './Components/common/Preloader/Preloader';
import { Provider } from 'react-redux';
import store from './Redux/redux-store';
import { withSuspense } from './Components/hoc/withSuspense';
import { Redirect ,Switch } from 'react-router-dom';

const ProfileConteiner = React.lazy(() => import('./Components/profile/PofileConteiner'))
const DialogsConteiner = React.lazy(() => import('./Components/Dialogs/DialogsConteiner'))
class App extends React.Component {
  catchAllUnhandledErrors = () => {
    alert('fgh')
  }

  componentDidMount(){
    this.props.initializeApp()
    window.addEventListener("unhandledrejection",this.catchAllUnhandledErrors)
  }
  componentWillUnmount() {
    window.removeEventListener("unhandledrejaction",this.catchAllUnhandledErrors) 
  }


  render() {
    if(!this.props.initialized){
      return <Preloader />
    }
    return (
      <div className="App">
        <HeaderConteiner />
        <Navbar />
        <div className='component'>
          <Switch>
          <Route exact path='/' 
            render={ () => <Redirect to={"/profile"}/>} />
          <Route path='/dialogs' 
            render={ withSuspense(DialogsConteiner) } />          
          <Route path='/profile/:userId?' 
            render={ withSuspense(ProfileConteiner) } />
          <Route path='/users' 
            render={ () => <UsersConteiner pageTitle={'samuray'} />} />
          <Route path='/login'
            render={ () => <LoginPage />} />
          <Route path='*'
            render={ () => <div>404 NOT FOUND</div>} />  
          </Switch>
        </div>          
      </div>
    )
  } 
}
let mapStateToProps = (state) => ({
  initialized:state.app.initialized
})

let AppConteiner = compose(
  withRouter,
  connect( mapStateToProps, { initializeApp }))(App);

let SamurayJSApp = (props) => {
  return <BrowserRouter>
    <Provider store={store}>
      <AppConteiner />
    </Provider>
  </BrowserRouter>
}

export default SamurayJSApp