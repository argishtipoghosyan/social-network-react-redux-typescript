import React from 'react';

import profileReducer, { addPostActionCreator, deletePost } from './profile-reducer';
let state = {
    posts:[
        {id:1,message:'fvvvdfssfdfcvfd', LikesCount:1},
        {id:2,message:'cc', LikesCount: 12},
        {id:3,message:'cc', LikesCount: 65},
        {id:4,message:'cc', LikesCount: 41},
    ],
}
test('length of posts should be incremented', () => {
    let action = addPostActionCreator('zczcxczcx')   
    let newState = profileReducer(state,action)
    expect(newState.posts.length).toBe(5)
});
test('message of new post should  be correct', () => {
    let action = addPostActionCreator('fddgghgdgds')   
    let newState = profileReducer(state,action)
    expect(newState.posts[4].message).toBe('fddgghgdgds')
});

test('renders learn react link', () => {
    let action = deletePost(1)
    let newState = profileReducer(state,action)
    expect(newState.posts.length).toBe(3)
});

test('renders learn react link', () => {
    let action = deletePost(1000)
    let newState = profileReducer(state,action)
    expect(newState.posts.length).toBe(4)
});