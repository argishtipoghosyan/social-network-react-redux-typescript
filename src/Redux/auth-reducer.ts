import { authAPI , securityAPI} from "../api/api"
import { stopSubmit } from "redux-form"

const SET_USER_DATA = 'SET_USER_DATA'
const GET_CAPTCHA_URL_SUCCES = 'GET_CAPTCHA_URL_SUCCES'


let initialState = {
    userId: null as number | null,
    email: null as string | null,
    login: null as string | null,
    isAuth: false,
    captchaUrl:null as string | null 
}

export type InitialStateType = typeof initialState 

const authReducer = (state = initialState , action:any): InitialStateType => {
    switch (action.type) {
        case SET_USER_DATA: 
        case GET_CAPTCHA_URL_SUCCES:
            return {
                ...state,
                ...action.payload,
            }
        default: 
            return state
    }
}
type SetAuthUserDataActionPayloadType = {
    userId:number | null,
    email:string | null,
    login:string | null,
    isAuth:boolean
}

type SetAuthUserDataActionType = {
    type: typeof SET_USER_DATA,
    payload : SetAuthUserDataActionPayloadType
} 

export const setAuthUserData = ( userId:number | null, email:string | null, login:string | null, isAuth:boolean ):SetAuthUserDataActionType => ({ 
    type: SET_USER_DATA,
     payload:{ userId, email, login, isAuth} 
})

type GetCaptchaUrlSuccesActionType = {
    type: typeof GET_CAPTCHA_URL_SUCCES
    payload:{ captchaUrl:string }
}
export const getCaptchaUrlSucces = (captchaUrl:string): GetCaptchaUrlSuccesActionType => ({ type: GET_CAPTCHA_URL_SUCCES, payload:{ captchaUrl } })

export const getAuthUserData = () => async (dispatch:any) => {
    let Response = await authAPI.me()
    if( Response.data.resultCode === 0 ){
        let {id , login , email} = Response.data.data
        dispatch(setAuthUserData( id, email, login, true))
    }
}  


export const login = (email:string , password:string , rememberMe:boolean , captcha:string ) => async (dispatch:any) => {  
    let Response = await authAPI.login(email ,password, rememberMe, captcha)
    if( Response.data.resultCode === 0 ){
        dispatch(getAuthUserData())
    }
    else {
        if( Response.data.resultCode === 10 ) {
            dispatch(getCaptchaUrl())
        }
        let message = Response.data.messages.length > 0 ? Response.data.messages[0] : "Some error"
        dispatch(stopSubmit('login',{_error: message }))
    }
}  

export const getCaptchaUrl = () => async (dispatch:any) => {  
    const Response = await securityAPI.getCaptchaUrl()
    const captchaUrl = Response.data.url
    dispatch(getCaptchaUrlSucces(captchaUrl))
}

export const logout = () => async (dispatch:any) => {
    let Response = await authAPI.logout()
    if( Response.data.resultCode === 0 ){
        dispatch(setAuthUserData( null, null, null, false))
    }
}  
export default authReducer