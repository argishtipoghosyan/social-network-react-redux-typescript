import profileReducer from "./profile-reducer"
import dialogsReducer from "./dialogs-reducer"
import sidebarReducer from "./sidebar-reducer"

let store = {    
    _state:{
        profilePage:{
            posts:[
            {id:1,message:'fvvvdfssfdfcvfd', LikesCount:1},
            {id:2,message:'cc', LikesCount: 12},
            {id:3,message:'cc', LikesCount: 65},
            {id:4,message:'cc', LikesCount: 41},
            ],
            NewPostText:'privet',
            },
        dialogsPage:{
        messages:[
            {message:'fvvvdfssfdfcvfd', id:1},
            {message:'cc', id: 2},
            {message:'cc', id: 3},
            {message:'cc', id: 4},
        ],
        dialogs:[
            {name:'Aram' , id:1},
            {name:'Armen' , id:2},
            {name:'Mane' , id:3},
            {name:'Nane' , id:4},
            {name:'Karine' , id:5},
            {name:'Anna' , id:6},
        ],
        newessageBody:''
        },
        sidebarsPage:{},
    },
    _renderTree() {
    },
    getState(){
        return this._state
    },
    subscribe(observer) {
        this._renderTree = observer
    },
    dispatch (action) {
        this._state.profilePage = profileReducer(this._state.profilePage , action)
        this._state.dialogsPage = dialogsReducer(this._state.dialogsPage , action)
        this._state.sidebar = sidebarReducer(this._state.sidebarsPage , action)
        this._renderTree(this._state)
    },
}





export default store
window.store = store