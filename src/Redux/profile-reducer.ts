import { usersAPI, profileAPI } from "../api/api"
import { stopSubmit } from "redux-form"
import {PostsType, ContactsType,PhotosType ,ProfileType} from '../types/types'

const ADD_POST = 'ADD-POST'
const SET_USERS_PROFILE = 'SET_USERS_PROFILE'
const SET_STATUS = 'SET_STATUS'
const DELETE_POST = 'DELETE_POST'
const SAVE_PHOTO_SUCCESS = 'SAVE_PHOTO_SUCCESS'




let initialState = {
    posts:[
        {id:1,message:'fvvvdfssfdfcvfd', LikesCount:1},
        {id:2,message:'cc', LikesCount: 12},
        {id:3,message:'cc', LikesCount: 65},
        {id:4,message:'cc', LikesCount: 41},
    ] as Array<PostsType>,
    profile: null as ProfileType | null,
    status:'',
    newPostText:''
}
 
export type InitialStateType = typeof initialState
 
const profileReducer = (state = initialState , action:any):InitialStateType => {
    switch (action.type) {
        case ADD_POST:{
            let newPost = {
                id:5, 
                message: action.newPostText,
                LikesCount:0
            }
            return {
                ...state,
                posts: [...state.posts, newPost],
            }   
        }
        case SET_STATUS:{
            return {
                ...state,
                status:action.status
            }
        }
        case SET_USERS_PROFILE:{
            return{
                ...state,
                profile:action.profile
            }
        }
        case DELETE_POST:{
            return {...state,posts:state.posts.filter(p=> p.id!= action.postId)}
        }
        case SAVE_PHOTO_SUCCESS:{
            return {...state , profile:{...state.profile,photos:action.photos} as ProfileType}
        }
        default: 
            return state
    }
}
type AddPostActionCreatorActionType = {
    type: typeof ADD_POST
    newPostText: string
}
export const addPostActionCreator = (newPostText:string):AddPostActionCreatorActionType => ({ type:ADD_POST ,newPostText})
type SetUsersProfileActionType = {
    type: typeof SET_USERS_PROFILE  
    profile: ProfileType
}
export const setUsersProfile = (profile:ProfileType):SetUsersProfileActionType => ({ type:SET_USERS_PROFILE , profile})
type SetStatusActionType = {
    type: typeof SET_STATUS
    status: string
}
export const setStatus = (status:string):SetStatusActionType => ({ type:SET_STATUS , status })
type DeletePostActionType = {
    type: typeof DELETE_POST
    postId:number
}
export const deletePost = (postId:number):DeletePostActionType => ({ type:DELETE_POST , postId })
type SavePhotoSuccessActionType = {
    type: typeof SAVE_PHOTO_SUCCESS
    photos:PhotosType
}
export const savePhotoSuccess = (photos:PhotosType):SavePhotoSuccessActionType => ({ type:SAVE_PHOTO_SUCCESS , photos })

export const getUsersProfile = (userId:number) => async (dispatch:any) => {
    let Response = await usersAPI.getProfile(userId)
    dispatch(setUsersProfile(Response.data))
}
export const getStatus = (userId:number) => async (dispatch:any) => {
    let Response = await profileAPI.getStatus(userId)
    dispatch(setStatus(Response.data))

}
export const updateStatus = (status:string) => async (dispatch:any) => {
    try{
        let Response = await profileAPI.updateStatus(status)        
        if (Response.data.resultCode === 0) {
            dispatch(setStatus(status))
        }
    } 
    catch(error){
        debugger
    }

}
export const savePhoto = (file:any) => async (dispatch:any) => {
    let Response = await profileAPI.savePhoto(file)        
    if (Response.data.resultCode === 0) {
        dispatch(savePhotoSuccess(Response.data.data.photos))
    }

}
export const saveProfile = (profile:ProfileType) => async (dispatch:any,getState:any) => {
    const userId = getState().auth.userId
    const Response = await profileAPI.saveProfile(profile)        
    if (Response.data.resultCode === 0) {
        dispatch(getUsersProfile(userId))
    }
    else {
        dispatch(stopSubmit('edit-profile',{_error: Response.data.messages[0] }))
        return Promise.reject(Response.data.messages[0])
    }

}
export default profileReducer