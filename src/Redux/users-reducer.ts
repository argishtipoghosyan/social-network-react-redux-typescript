import { usersAPI }from '../api/api'
import { updateObjectInArray } from '../utils/object-helpers'
import {UserType} from '../types/types'
import { AppStateType } from './redux-store'
import { Dispatch } from 'redux'
import { ThunkAction } from 'redux-thunk'
const FOLLOW = 'FOLLOW'
const UNFOLLOW = 'UNFOLLOW'
const SET_USERS =  'SET_USERS'
const SET_CURRENT_PAGE =  'SET_CURRENT_PAGE'
const SET_TOTAL_USERS_COUNT =  'SET_TOTAL_USERS_COUNT'
const TOOGL_IS_FATCHING =  'TOOGL_IS_FATCHING'
const TOOGL_FOLLOWING_IN_PROCESS =  'TOOGL_FOLLOWING_IN_PROCESS'



let initialState = {
    users:[] as Array<UserType>,
    currentPage:1,
    totalUsersCount:0,
    pageSize:10,
    isFatching:true,
    followingInProcess:[] as Array<number>
}

type InitialState = typeof initialState


const usersReducer = (state = initialState , action:ActionTypes):InitialState => {
    switch (action.type) {
        case FOLLOW: {
            return {
                ...state,
                users:updateObjectInArray(state.users, action.userId, 'id', {followed: true} )
            }
        }
        case UNFOLLOW: {
            return {
                ...state,
                users:updateObjectInArray(state.users, action.userId, 'id', {followed: false} )
            }
        }
        case SET_USERS: {
            return {...state,users:action.users}
        }
        case SET_CURRENT_PAGE: {
            return {...state,currentPage: action.currentPage }
        }
        case SET_TOTAL_USERS_COUNT: {
            return {...state,totalUsersCount: action.count }
        }
        case TOOGL_IS_FATCHING: {
            return {...state,isFatching: action.isFatching }
        }
        case TOOGL_FOLLOWING_IN_PROCESS: {
            return {...state,
                followingInProcess:action.isFatching 
                ? [...state.followingInProcess, action.userId ] 
                : state.followingInProcess.filter(id => id != action.userId)
            }
        }
        default: 
            return state
    }
}
type ActionTypes = FollowSuccesActionType | UnfollowSuccesActionType
| SetUsersActionType | SetCurrentPageActionType | SetTotalUsersCountActionType
| ToogleIsFatchingActionType | TooglefollowingInProcessActionType

type FollowSuccesActionType ={
    type:typeof FOLLOW   
    userId:number
}
export const followSucces = (userId:number):FollowSuccesActionType => ({ type:FOLLOW , userId })
type UnfollowSuccesActionType ={
    type:typeof UNFOLLOW   
    userId:number
}
export const unfollowSucces = (userId:number):UnfollowSuccesActionType => ({ type:UNFOLLOW , userId })
type SetUsersActionType ={
    type:typeof SET_USERS
    users:Array<UserType>
}
export const setUsers = (users:Array<UserType>):SetUsersActionType => ({ type:SET_USERS , users })
type SetCurrentPageActionType ={
    type:typeof SET_CURRENT_PAGE   
    currentPage:number
}
export const setCurrentPage = (currentPage:number):SetCurrentPageActionType => ({ type:SET_CURRENT_PAGE , currentPage })
type SetTotalUsersCountActionType ={
    type:typeof SET_TOTAL_USERS_COUNT   
    count:number
}
export const setTotalUsersCount = (totalUsersCount:number):SetTotalUsersCountActionType => ({ type:SET_TOTAL_USERS_COUNT , count:totalUsersCount })
type ToogleIsFatchingActionType ={
    type:typeof TOOGL_IS_FATCHING   
    isFatching:boolean
}
export const ToogleIsFatching = (isFatching:boolean):ToogleIsFatchingActionType => ({ type: TOOGL_IS_FATCHING, isFatching })
type TooglefollowingInProcessActionType ={
    type:typeof TOOGL_FOLLOWING_IN_PROCESS   
    isFatching :boolean
    userId:number
}
export const TooglefollowingInProcess = (isFatching :boolean, userId :number):TooglefollowingInProcessActionType => ({ type: TOOGL_FOLLOWING_IN_PROCESS, isFatching, userId })


type GetStateType = () => AppStateType
type DispatchType = Dispatch<ActionTypes>
type ThunkType = ThunkAction<Promise<void>, AppStateType, unknown,ActionTypes >

export const requestUsers = (page:number,pageSize:number):ThunkType=> {
    return async (dispatch, getState) => {
        dispatch(setCurrentPage(page))
        dispatch(ToogleIsFatching( true ))

        let data = await usersAPI.getUsers(page,pageSize)
        dispatch(ToogleIsFatching( false ))
        dispatch(setUsers(data.items))
        dispatch(setTotalUsersCount(data.totalCount))
    }
}


const _followUnfollowFlow = async (dispatch:DispatchType,
                                    apiMethod:any, 
                                    userId:number, 
                                    actionCreator:(userId:number)=> FollowSuccesActionType | UnfollowSuccesActionType ) => {
    dispatch(TooglefollowingInProcess(true, userId))
        let Response = await apiMethod(userId)
        if(Response.data.resultCode == 0){
            dispatch(actionCreator(userId))
        }
        dispatch(TooglefollowingInProcess(false, userId))
}

export const follow = (userId:number):ThunkType => {
    return async (dispatch) => {
        _followUnfollowFlow(dispatch, usersAPI.follow.bind(usersAPI), userId, followSucces)
    }
}
export const unfollow = (userId:number):ThunkType => {
    return async (dispatch) => {
        _followUnfollowFlow(dispatch, usersAPI.unfollow.bind(usersAPI), userId, unfollowSucces)
    }
}
export default usersReducer