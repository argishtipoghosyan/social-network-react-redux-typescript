const SEND_MESSAGE = 'SEND-MESSAGE'

type DialogType = {
    name: string
    id:number
}
type MessageType = {
    message: string
    id:number
}
let initialState = {
    messages:[
        {message:'fvvvdfssfdfcvfd', id:1},
        {message:'cc', id: 2},
        {message:'cc', id: 3},
        {message:'cc', id: 4},
    ] as Array<MessageType>,
    dialogs:[
        {name:'Aram' , id:1},
        {name:'Armen' , id:2},
        {name:'Mane' , id:3},
        {name:'Nane' , id:4},
        {name:'Karine' , id:5},
        {name:'Anna' , id:6},
    ] as Array<DialogType>,
    
}

export type InitialStateType = typeof initialState 

const dialogsReducer = (state = initialState, action:any):InitialStateType => {
    switch (action.type) {
        case SEND_MESSAGE :{
            let body = action.newmessageBody
            return{
                ...state,
                messages:[...state.messages,{id: 5,message: body }],
            }
        }
        default:    
            return state
    }
}

type sendMessageCreatorActionType = {
    type: typeof SEND_MESSAGE
    newmessageBody : string

}
export const sendMessageCreator = (newmessageBody:string):sendMessageCreatorActionType => ({ type:SEND_MESSAGE , newmessageBody})

export default dialogsReducer


