import React from 'react';
import s from './Dialogs.module.css'
import Message from './Message/Message';
import DialogItem from './DialogItm/DialogItem';
import { Field, reduxForm } from 'redux-form'
import { Textarea } from '../common/FormsControl/FormsControl';
import { required, maxLengthCreator } from '../../utils/validators/validators';

const Dialogs = (props) => {
    let state = props.dialogsPage
    let DialogsElements = state.dialogs.map((d,i) => <DialogItem key={i} name={d.name} id={d.id} />)
    let MessagesElements = state.messages.map((k,i) => <Message key={i} message={k.message} id={k.id} />)
    
    let addNewMessage = (values) => {
        props.sendMessage(values.newMessageBody)
    }

    return(
        <div className={s.dialogs}>
            <div className={s.dialog}>
                { DialogsElements }
            </div>
            <div className={s.message}>
                { MessagesElements }
            </div>
            <div>
                <AddMessageFormRedux onSubmit={addNewMessage} />
            </div>
        </div>
    );
}

const maxLength100 = maxLengthCreator(100)
const AddMessageForm = (props) => {
    return (
        <form onSubmit={props.handleSubmit} >
            <div>
                <Field placeholder='enter your message' name='newMessageBody' 
                    component={Textarea} validate={[required, maxLength100 ]} />
            </div>
            <div>
                <button>send</button>
            </div>
        </form>
    )   
}

const AddMessageFormRedux = reduxForm ({form:'dialogAddMessageForm'})(AddMessageForm)

export default Dialogs;