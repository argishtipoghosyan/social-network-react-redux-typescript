import React from 'react';
import { sendMessageCreator } from '../../Redux/dialogs-reducer';
import Dialogs from './Dialogs';
import { connect } from 'react-redux';
import { withAuthRedirect } from '../hoc/withAuthRedirect';
import { compose } from 'redux';

const mapDispatchToProps = (dispatch) => {
    return {
        sendMessage: (newmessageBody) =>{
            dispatch(sendMessageCreator(newmessageBody))
        }
    }
}

const mapStateToProps = (state) => {
    return {
        dialogsPage:state.dialogsPage,
    }
}

export default compose (
    connect (mapStateToProps,mapDispatchToProps),
    withAuthRedirect
)(Dialogs)
