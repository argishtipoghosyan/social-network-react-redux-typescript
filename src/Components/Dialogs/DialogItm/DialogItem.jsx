import React from 'react';
import { NavLink } from 'react-router-dom';
import s from './DialogItem.module.css'

const DialogItem = (props) => {
    return (
        <div className={s.item}>
            <NavLink to={'/dialogs/' + props.id} activeClassName={s.itemactiv} >{props.name}</NavLink>
        </div>
    );
}

export default DialogItem