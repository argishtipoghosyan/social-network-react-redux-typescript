import React from 'react'; 
import prelorder from '../../../image/preloader.gif'
import s from './Preloader.module.css'

let Preloader =  (props) => {
    return <div><img src={ prelorder } className={s.preloader} /></div>
}

export default Preloader