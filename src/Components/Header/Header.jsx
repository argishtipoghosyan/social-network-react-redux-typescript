import React from 'react';
import s from './Header.module.css'
import { NavLink } from 'react-router-dom';


const Header = (props) => {
    return(
        <div className='header'>
            <img src={require('../../image/bool.png')} className={s.logo} />
            <div className={ s.loginBlock } >
                { props.isAuth ?<div>{props.email}- <button onClick={props.logout} >Log out</button> </div>  : <NavLink to={ '/login' } >Login</NavLink> }       
            </div>
        </div>
    );
 }
 
 export default Header;