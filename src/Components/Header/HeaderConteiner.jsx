import React from 'react';
import Header from './Header'
import { connect } from 'react-redux';
import {logout } from '../../Redux/auth-reducer'


class HeaderConteiner extends React.Component {
    render() {
        
        return <Header {...this.props}/>
    }
}

let mapStateToProps = (state) => ({
    isAuth:state.auth.isAuth,
    email:state.auth.email
})


export default connect( mapStateToProps, { logout }) (HeaderConteiner);