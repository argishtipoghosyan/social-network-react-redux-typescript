import React from 'react';
import { NavLink } from 'react-router-dom';
import s from './Navbar.module.css'

const Navbar = () => {

     return(
        <div className='nav'>
            <div className={s.diolog}>
               <NavLink to='/profile' activeClassName={s.activ}>Profile</NavLink>
            </div>
            <div className={s.diolog}>
               <NavLink to='/dialogs' activeClassName={s.activ}>Message</NavLink>
            </div>
            <div className={s.diolog}>
               <NavLink to='/users' activeClassName={s.activ}>Users</NavLink>
            </div>
            <div className={s.diolog}>
               <NavLink to='' >News</NavLink>
            </div>
            <div className={s.diolog}>
               <NavLink to='' >Music</NavLink>
            </div>
            <div className={s.diolog}>
               <NavLink to='' >Settings</NavLink>
            </div>
        </div>
     );
 }
 
 export default Navbar;