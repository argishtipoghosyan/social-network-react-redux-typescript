import React from 'react';
import {  addPostActionCreator } from '../../../Redux/profile-reducer';
import Mypost from './Mypost';
import { connect } from 'react-redux';

const mapDispatchToProps = (dispatch) => {
    return {
        addPost: (newPostText) => {
            dispatch(addPostActionCreator(newPostText))
        }
    }
}

const mapStateToProps = (state) => {
    return {
        posts:state.profilePage.posts,
    }
}

const MypostConteiner = connect(mapStateToProps,mapDispatchToProps) (Mypost)
export default MypostConteiner