import React from 'react';
import s from './mypost.module.css'
import Post from './Posts/Post'
import { Field, reduxForm } from 'redux-form'
import { required, maxLengthCreator } from '../../../utils/validators/validators';
import { Textarea } from '../../common/FormsControl/FormsControl';

const Mypost = React.memo(props => {
    let postsElements = [...props.posts].reverse().map(d => <Post key={d.id} message={ d.message } LikesCount={ d.LikesCount } />)
    
    let onAddPost = (values) => {
        props.addPost(values.newPostText);
    }

    return (
        <div >
            <h3>My Posts</h3>
            <AddNewPostFormRedux onSubmit={onAddPost} />
            <div>
                { postsElements }
            </div>
        </div>
    )
    
})
const maxLength10 = maxLengthCreator(10)
const AddNewPostForm = (props) => {
    return(
        <form onSubmit={props.handleSubmit} >
            <div>
                <Field name='newPostText' placeholder={"Post message"} 
                    component={Textarea} validate={[required, maxLength10 ]} />
            </div>
            <div>
                <button>Add post</button>
            </div>
        </form>
    )
}

const AddNewPostFormRedux = reduxForm({form:'profileAddNewPostForm'})(AddNewPostForm)

export default Mypost