import React from 'react';
import s from './Profile.module.css'
import ProfileInfo from './ProfileInfo/ProfileInfo';
import MypostConteiner from './Mypost/MypostConteiner';
import { Redirect } from 'react-router-dom';


function Profile(props) {
  
  return (
    <div className={ s.mess }>
      <ProfileInfo savePhoto={props.savePhoto} isOwner={props.isOwner}
      saveProfile={props.saveProfile} 
      profile={props.profile} updateStatus={props.updateStatus} status={props.status} />
      <MypostConteiner/>
    </div>
  )

}

export default Profile;
