import React from 'react';
import { getUsersProfile, getStatus, updateStatus, savePhoto, saveProfile } from '../../Redux/profile-reducer';
import Profile from './Profile';
import { connect } from 'react-redux';
import { withRouter, Redirect } from 'react-router-dom';
import { compose } from 'redux';


class ProfileConteiner extends React.Component {
    refreshProfile(){
        let userId = this.props.match.params.userId
        if (!userId) {
            userId = this.props.authorisedUserId
            if(!userId){
                this.props.history.push('/login')
            }
        }
        this.props.getUsersProfile(userId)
        this.props.getStatus(userId)

    }
    componentDidMount(){
        this.refreshProfile()
    }
    componentDidUpdate(prevProps,prevState,snapshot){
        if(this.props.match.params.userId!=prevProps.match.params.userId){
            this.refreshProfile()
        }
    }

    render(){
        
        return (
            <Profile {...this.props} savePhoto={this.props.savePhoto} isOwner={!this.props.match.params.userId} profile={this.props.profile} updateStatus={this.props.updateStatus} status={this.props.status}/>
        );     
    }
}

let mapStateToProps = (state) => ({
    profile:state.profilePage.profile,
    status:state.profilePage.status,
    isAuth:state.auth.isAuth,
    authorisedUserId:state.auth.userId
})
export default compose (
    connect(mapStateToProps , { getUsersProfile ,getStatus,updateStatus, savePhoto,saveProfile}),
    withRouter,
)(ProfileConteiner)
