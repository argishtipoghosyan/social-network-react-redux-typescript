import React ,{useState, useEffect} from 'react';

const ProfileStatusWithHooks = (props) => {
    let [editMode, SetEditMode] = useState(false) 
    let [status, setStatus] = useState(props.status) 

    useEffect( () => {
        setStatus(props.status)
    },[props.status])
    const actvateEditMode = () => {
        SetEditMode (true)
    }
    const deactvateEditMode = () => {
        SetEditMode (false)    
        props.updateStatus(status)
    }
    const onStatusChange = (e) => {
        setStatus( e.currentTarget.value )
        
    }
    return (
        <div>
            { ! editMode &&
                <div>
                    <b>Status:</b><span onDoubleClick = { actvateEditMode }>{props.status || '-----'}</span>
                </div> 
            }
            { editMode &&
                <div>
                    <input value={status} onChange={ onStatusChange } autoFocus = { true } onBlur = { deactvateEditMode }/>
                </div>
            }
        </div>  
    )
}

export default ProfileStatusWithHooks