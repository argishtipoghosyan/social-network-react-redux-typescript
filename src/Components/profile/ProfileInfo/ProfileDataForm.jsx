import React from 'react';
import { createField , Input, Textarea} from '../../common/FormsControl/FormsControl'
import { reduxForm } from 'redux-form'
import s from './ProfileInfo.module.css'
import style from '../../common/FormsControl/FormsControl.module.css'


const ProfileDataForm = ({handleSubmit,profile,error}) => {
    return <form onSubmit={handleSubmit} >
    	<div><button>Save</button></div>
        {error && <div className={style.formSummerError}>  
            { error }
        </div>}
    	<div>
            <b>Full Name</b>: {createField( 'Full name', 'fullName', [], Input )}
        </div>
        <div>
            <b>Loking forr a jon</b>:{ createField('','lookingForAJob',[],Input,{type:'checkbox'}) }
        </div>
        <div>
            <b>My professional skills</b> :{ createField('My professional skills','lookingForAJobDescription',[],Textarea) }        	
        </div>
        <div>
            <b>About me</b> :{ createField('About me','aboutMe',[],Textarea) }
        </div>
        <div>
            <b>contacts</b> :{Object.keys(profile.contacts).map(key => {
                return <div key={key} className={s.contact}>
                    <b>{key}:{ createField(key ,'contacts.'+ key,[],Input) }</b>
                </div>
            })}
        </div>


    </form>
}
const ProfileDataFormReduxForm = reduxForm({form:'edit-profile'})(ProfileDataForm)
export default ProfileDataFormReduxForm