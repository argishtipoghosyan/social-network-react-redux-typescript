import React,{ useState } from 'react';
import s from './ProfileInfo.module.css'
import photoUser from '../../../image/avatar.png'
import Preloader from '../../common/Preloader/Preloader';
import ProfileStatusWithHooks from './ProfileStatusWithHooks';
import ProfileDataForm from './ProfileDataForm'

const ProfileInfo = ({profile, updateStatus, status, isOwner ,savePhoto,saveProfile}) => {
    let [editMode, SetEditMode] = useState(false) 
    if(!profile){
        return <Preloader />
    }
    const onMainPhotoSelected = (e) => {
        if(e.target.files.length){
            savePhoto(e.target.files[0])
        }
    }
    const onSubmit = (formData) => {
        saveProfile(formData).then(
            () => {
                SetEditMode(false) 
            }
        )
    }
    
    return (
        <div>
            <div >     
                <img src={ profile.photos.large!= null ? profile.photos.large : photoUser } className={s.avatar} />
                {isOwner && <input type={'file'} onChange={onMainPhotoSelected} />}
                {editMode ? <ProfileDataForm onSubmit={onSubmit} initialValues={profile} profile={profile} /> : <ProfileData goToEditMode={() => {SetEditMode(true)}} profile={profile} isOwner={isOwner} /> }
                <ProfileStatusWithHooks status ={status} updateStatus ={ updateStatus } />
            </div>
        </div>
    );
}

const Contact = ({contactTitle,contactValue}) => {
    return <div className={s.contact} ><b>{ contactTitle}</b>: {contactValue}</div>
}

const ProfileData = ({profile,isOwner,goToEditMode}) => {
    return <div>
        { isOwner && <div><button onClick={goToEditMode} >edit</button></div>}
                    <div>
                        <b>Full Name</b>: { profile.fullName }
                    </div>
                    <div>
                        <b>Loking for a jon</b>:{ profile.lookingForAJob ? 'yes' : 'no'}
                    </div>
                    {profile.lookingForAJob &&
                    <div>
                        <b>My professional skills</b> :{profile.lookingForAJobDescription}
                    </div>
                    }
                    <div>
                        <b>About me</b> :{profile.aboutMe}
                    </div>
                    <div>
                        <b>contacts</b> :{Object.keys(profile.contacts).map(key => {
                            return <Contact key={key} contactTitle={key} contactValue={profile.contacts[key]} />
                        })}
                    </div>
                </div>
}

export default ProfileInfo