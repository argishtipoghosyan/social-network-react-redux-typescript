import React, { FC } from 'react';
import Paginator from '../common/Paginator/Paginator';
import User from './User';
import { UserType } from '../../types/types'

type PropsType = {
    currentPage:number
    onPageChenged: (pageNumber:number) => void
    totalUsersCount:number
    pageSize:number
    users:Array<UserType>
    followingInProcess:Array<number>
    unfollow: (userId:number) => void
    follow: (userId:number) => void
}

let Users:FC<PropsType> = ({currentPage,onPageChenged ,pageSize,totalUsersCount,users,...props}) => {
    return <div>
       
        <Paginator currentPage={currentPage} onPageChenged={onPageChenged} totalItemsCount={totalUsersCount} pageSize={pageSize} />
        {
        users.map(u => <User key={u.id} user={u}  followingInProcess={props.followingInProcess} unfollow={props.unfollow} follow={props.follow} />)
        }
    </div>
}

export default Users