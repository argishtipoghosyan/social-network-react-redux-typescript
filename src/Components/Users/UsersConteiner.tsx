import React from 'react';
import { connect } from 'react-redux';
import { unfollow , requestUsers , follow  } from '../../Redux/users-reducer';
import Users from './Users';
import s from './Users.module.css'
import Preloader from '../common/Preloader/Preloader';
import { compose } from 'redux';
import { getCurrentPage, getUsers, getPageSize, getTotalUsersCount, getIsFatching, getFollowingInProcess  } from '../../Redux/users-selector';
import { UserType } from '../../types/types'
import { AppStateType } from '../../Redux/redux-store'

type MapStatePropsType = {
    currentPage:number
    pageSize:number
    isFatching:boolean
    totalUsersCount:number
    users:Array<UserType>
    followingInProcess:Array<number>
}
type MapDispatchPropsType = {
    requestUsers: (currentPage:number, pageSize:number) => void
    unfollow: (userId:number) => void
    follow: (userId:number) => void
}
type OunPropsType = {
    pageTitle:string
}

type PropsType = MapStatePropsType & MapDispatchPropsType & OunPropsType

class UsersConteiner extends React.Component<PropsType> {
    componentDidMount(){
        const {currentPage, pageSize} = this.props
        this.props.requestUsers(currentPage,pageSize)
    }
    onPageChenged = (pageNumber:number) => {    
        const {pageSize} = this.props
        this.props.requestUsers(pageNumber,pageSize)
    }
    render() {
        return <>
        <h2>{this.props.pageTitle}</h2>
        {this.props.isFatching ? <Preloader /> : null }
        <Users   currentPage = { this.props.currentPage }
                pageSize = { this.props.pageSize }
                totalUsersCount = { this.props.totalUsersCount }
                users = { this.props.users }
                follow = { this.props.follow }
                unfollow = { this.props.unfollow }
                onPageChenged = { this.onPageChenged }
                followingInProcess = { this.props.followingInProcess }
        /> 
        </> 
    }
}

const mapStateToProps = (state:AppStateType):MapStatePropsType => {
    return {
        users:getUsers(state),
        currentPage:getCurrentPage(state),
        pageSize:getPageSize(state),
        totalUsersCount:getTotalUsersCount(state),
        isFatching:getIsFatching(state),
        followingInProcess:getFollowingInProcess(state)
    }
}


export default compose(
    connect<MapStatePropsType,MapDispatchPropsType,OunPropsType,AppStateType>(mapStateToProps,
    { follow , unfollow ,  requestUsers}) 
)(UsersConteiner)
